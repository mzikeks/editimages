import cv2
import numpy as np
from random import randint

icon = cv2.imread('./icons/44.png', cv2.IMREAD_UNCHANGED)

image = cv2.imread('./images/22.jpg', cv2.IMREAD_UNCHANGED)
image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)

overlay_icon = np.zeros((image.shape[0], image.shape[1], 4), dtype="uint8")
overlay_icon[:icon.shape[0], :icon.shape[1]] = icon

Bi, Gi, Ri, Ai = cv2.split(image)
Bw, Gw, Rw, Aw = cv2.split(overlay_icon)

Bi = cv2.bitwise_and(Bi, Bi, mask=255-Aw)
Gi = cv2.bitwise_and(Gi, Gi, mask=255-Aw)
Ri = cv2.bitwise_and(Ri, Ri, mask=255-Aw)

Bw = cv2.bitwise_and(Bw, Bw, mask=Aw)
Gw = cv2.bitwise_and(Gw, Gw, mask=Aw)
Rw = cv2.bitwise_and(Rw, Rw, mask=Aw)

image2 = cv2.merge([Bi+Bw, Gi+Gw, Ri+Rw])

cv2.imwrite("Combine Image.jpg", image2)


