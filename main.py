from os import listdir
import cv2
import time
from random import randint
import numpy as np
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPM
from albumentations import (GaussNoise, Blur)


table = open('coords.csv', 'a')
image_albs = [Blur(blur_limit=73, p=1)]  # набор альбументаций для фонов
icon_albs = [GaussNoise(p=1)]  # набор альбументаций для значков

icons_svg_path = './icons_svg/'  # папка с svg (не используется)
icons_path = './icons/'  # папка с значками png
images_path = './images/'  # папка с исходными картинкам
output_path = './res/'  # папка с резульатами работы

n_icons = (10, 30)  # число значков, от и до
scale_percents = (5, 10)  # границы масштабирования в процентах
rotation_deg = (-180, 180)

begin_time = time.time()

# Перевод иконок в png
#for icon_name in [f for f in listdir(icons_svg_path)]:
#    icon = svg2rlg(icons_svg_path+icon_name)
#    renderPM.drawToFile(icon, icons_path+icon_name.replace('.svg', '.png'), fmt="PNG")

icons = [f for f in listdir(icons_path)]
images = [f for f in listdir(images_path)]

for n_image, image_name in enumerate(images):
    image = cv2.imread(images_path+image_name, cv2.IMREAD_UNCHANGED)
    for alb in image_albs:
        image = alb(image=image)['image']

    if image.shape[2] != 4:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)

    h_image, w_image = image.shape[0], image.shape[1]
    logos = []

    for i in range(randint(n_icons[0], n_icons[1])):
        icon_name = icons[randint(0, len(icons)-1)]
        icon = cv2.imread(icons_path+icon_name, cv2.IMREAD_UNCHANGED)

        scale_percent = randint(scale_percents[0], scale_percents[1])
        h_icon, w_icon = int(icon.shape[0] * scale_percent / 100), int(icon.shape[1] * scale_percent / 100)
        icon = cv2.resize(icon, (w_icon, h_icon), interpolation=cv2.INTER_AREA)

        for alb in icon_albs:
            icon = alb(image=icon)['image']

        if icon.shape[2] != 4:  # иконка с фоном
            icon = cv2.cvtColor(icon, cv2.COLOR_BGR2BGRA)

        center = (icon.shape[1]//2, icon.shape[0]//2)
        rotation = cv2.getRotationMatrix2D(center, randint(rotation_deg[0], rotation_deg[1]), 0.7)
        icon0 = cv2.warpAffine(icon, rotation, (w_icon, h_icon), borderValue=(0, 0, 0))

        n_attempts = 0
        while True:
            n_attempts += 1
            from_x = randint(0, w_image-w_icon)
            from_y = randint(0, h_image-h_icon)
            m = True
            for el in logos:
                ll = from_x < el[0] and from_x + w_icon < el[0]
                rr = from_x > el[2] and from_x + w_icon > el[2]
                uu = from_y < el[1] and from_y + h_icon < el[1]
                dd = from_y > el[3] and from_y + h_icon > el[3]
                if ll + rr + uu + dd == 0:
                    m = False
                    break
            if n_attempts > 25000:
                print('Значки не влезают, попробуйте уменьшить масштаб или ограничить число')
                break
            if m:
                break

        overlay_icon = np.zeros((h_image, w_image, 4), dtype='uint8')
        overlay_icon[from_y:from_y + h_icon, from_x:from_x + w_icon] = icon0
        Bi, Gi, Ri, Ai = cv2.split(image)
        Bw, Gw, Rw, Aw = cv2.split(overlay_icon)

        Bi = cv2.bitwise_and(Bi, Bi, mask=255 - Aw)
        Gi = cv2.bitwise_and(Gi, Gi, mask=255 - Aw)
        Ri = cv2.bitwise_and(Ri, Ri, mask=255 - Aw)

        Bw = cv2.bitwise_and(Bw, Bw, mask=Aw)
        Gw = cv2.bitwise_and(Gw, Gw, mask=Aw)
        Rw = cv2.bitwise_and(Rw, Rw, mask=Aw)

        image = cv2.merge([Bi + Bw, Gi + Gw, Ri + Rw, np.ones((h_image, w_image), dtype='uint8')*255])

        table.write(f'{image_name};{icon_name};{from_x};{from_y};{from_x+w_icon};{from_y+h_icon}\n')
        logos.append([from_x, from_y, from_x+w_icon, from_y+h_icon])

    cv2.imwrite(output_path+image_name, image)
    print(f'Обработана {n_image+1} картинка из {len(images)}, прошло {time.time()-begin_time} секунд')

print(f'Обработано:  {len(images)} картинок, затрачено {time.time()-begin_time} секунд')